

// Base Class
class Parent {
	void eat()
	{
		System.out.println("\nEating()");
	}
}

// Inherited class
class Child extends Parent {
	void eat()
	{
		System.out.println("\nChild's Eating()");
	}
}

class Main {
	public static void main(String[] args){

    
		Parent obj1 = new Parent();
		obj1.eat();
		Parent obj2 = new Child();
		obj2.eat();
	}
}
