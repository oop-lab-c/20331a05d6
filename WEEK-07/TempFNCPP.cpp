#include<iostream>
using namespace std;
template<class T>
T mul(T &a,T &b,T &c)
{
    T product = a*b*c;
    return product;
}
int main()
{
    int a1=5;
    int a2=4;
    int a3=2;
    double m1=2.3;
    double m2=3.0;
    double m3=1.267;
    float b1=6.5;
    float b2=2.5;
    float b3=1.25;
    cout<<"the product of a1 ,a2  and a3 is "<<mul(a1,a2,a3)<<endl;
    cout<<"the product of m1,m2 and m3 is "<<mul(m1,m2,m3)<<endl;
    cout<<"product of b1,b2 and b3 is "<<mul(b1,b2,b3)<<endl;
    return 0;
}