import java.util.*;
abstract class A{
	public void display() {
		System.out.println("Welcome");
	}
}

class b extends A{
	public void display() {
		System.out.println("Derived");
		
	}
}
public class PartialAbstraction{

	public static void main(String[] args ) {
		
		b obj= new b();
		obj.display();
	}
}