#include<iostream>
using namespace std;
class Base{
    public:
    virtual void display()=0;
};
class derived1:public Base{
    public:
    void display(){
        cout<<"Derived1 class"<<endl;
        
    }
};
class derived2:public Base{
    public:
    void display(){
        cout<<"Derived2 class"<<endl;
    }
};
int main(){
    derived1 obj1;
    derived2 obj2;
    obj1.display();
    obj2.display();
return 0;
}
