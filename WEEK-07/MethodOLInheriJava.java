class ParentClass
{
   
   public void msg()
   {
      System.out.println("Parent class");
   }
}
class ChildClass extends ParentClass
{
       
    public void msg()
    {
         System.out.println("child class");
         System.out.println("overriding the method");
    }
}
    class Main{
   public static void main( String args[]) {
       ParentClass obj = new ChildClass();
       obj.msg();
   }
}