#include<iostream>
using namespace std;
class A {
   public:
      virtual void p() = 0; // Pure Virtual Function
};

class B:public A {
   public:
      void p() {
         cout << "Virtual Function \n";
      }
};

int main() {
   B obj;
   obj.p();
}