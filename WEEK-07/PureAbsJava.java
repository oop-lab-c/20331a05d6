interface Animal {
  public void bark(); 
  public void walk();
}
class Dog implements Animal {
  public void bark() {
    System.out.println("bow...bow...");
  }
  public void walk() {
    System.out.println("dog is walking");
  }
}

class Main {
  public static void main(String[] args) {
    Dog obj = new Dog();
    obj.bark();
    obj.walk();
  }
}
