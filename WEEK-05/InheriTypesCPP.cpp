#include<iostream>
using namespace std;
class A
{
    public:
    A()
    {
        cout<<"PYTHON"<<endl;
    }
};
class B
{
    public:
    B()
    {
        cout<<"JAVA"<< endl;
    }
};
class child1:public A //simple inheritance
{
    public:
    child1()
    {
        cout<<"iAM LEARNING PYTHON"<<endl;
    }
};
class child2:public A,public B //multiple inheritance
{
    public:
};
class child3:public child1//multilevel inheritance
{
    public:
};
class child4:public A//hiracheal inheritance
{
    public:
};
class child5:public A //hiracheal inheritance
{
    public:
};
int main()
{
    child1 obj1;
    child2 obj2;
    child3 obj3;
    child4 obj4;
    child5 obj5;
    return 0;
}
