class Animal
{
    void bark()
    {
        System.out.println("Barking");
    }
}
class Dog extends Animal
{
    void bark1()
    {
        System.out.println("bow..bow");
    }
}
class Puppie extends Dog // multiple inheitance 
{
    public static void main(String[] args)
    {
        Puppie p = new Puppie();
        p.bark();
        p.bark1();
    }
}
class Cat extends Animal // simple inheritance
{
    void bark2()
    {
        System.out.println("meow..meow");
    }
    public static void main(String[] args)
    {
        Cat c = new Cat();
        c.bark();
        c.bark2();
    }
}