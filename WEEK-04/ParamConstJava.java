class Student
{
    String fullName;
    double semPercentage;
    String collegeName;
    int collegeCode;
    Student()
    {
         collegeName = "MVGR";
         collegeCode = 33;
         System.out.println("College name is "+collegeName);
         System.out.println("college code is "+collegeCode);
    }
     Student(String n, double s)
    {
        
        fullName=n;
        semPercentage=s;
    }
    void display()
    {
        System.out.println("name is "+fullName);
        System.out.println("sem percentage is "+semPercentage);
    }
    public static void main(String[] args)  
    {
        Student obj = new Student();
        Student obj1 = new Student("sandhya",85.89);
        obj1.display();
    }
}
